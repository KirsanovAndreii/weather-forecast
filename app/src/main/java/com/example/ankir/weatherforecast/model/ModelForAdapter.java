package com.example.ankir.weatherforecast.model;

import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;

/**
 * Created by ankir on 01.07.2017.
 */

public class ModelForAdapter implements Serializable {
    private String date;
    private String icon_wether;
    private String weather;
    private String temperature;
    private String temperature_howfeeling;
    private String atm_pressure;
    private String sunrise;
    private String sunset;
    private String moonrise;
    private String moonset;
    private String humidity; //влажность
    private String cloudcover; // облачность
    private String chanceofrain; //вероятность дождя
    private String chanceoffog; // вероятность тумана
    private String wind;

    public ModelForAdapter() {
    }

    public ModelForAdapter(String icon_wether, String weather, String temperature, String temperature_howfeeling,
                           String atm_pressure, String sunrise, String sunset, String moonrise, String moonset,
                           String humidity, String cloudcover, String chanceofrain, String chanceoffog, String wind) {
        this.icon_wether = icon_wether;
        this.weather = weather;
        this.temperature = temperature;
        this.temperature_howfeeling = temperature_howfeeling;
        this.atm_pressure = atm_pressure;
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.moonrise = moonrise;
        this.moonset = moonset;
        this.humidity = humidity;
        this.cloudcover = cloudcover;
        this.chanceofrain = chanceofrain;
        this.chanceoffog = chanceoffog;
        this.wind = wind;
    }

    public String getDate() {
        return date;
    }

    public String getIcon_wether() {
        return icon_wether;
    }

    public String getWeather() {
        return weather;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getTemperature_howfeeling() {
        return temperature_howfeeling;
    }

    public String getAtm_pressure() {
        return atm_pressure;
    }

    public String getSunrise() {
        return sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public String getMoonrise() {
        return moonrise;
    }

    public String getMoonset() {
        return moonset;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getCloudcover() {
        return cloudcover;
    }

    public String getChanceofrain() {
        return chanceofrain;
    }

    public String getChanceoffog() {
        return chanceoffog;
    }

    public String getWind() {
        return wind;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setIcon_wether(String icon_wether) {
        this.icon_wether = icon_wether;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public void setTemperature_howfeeling(String temperature_howfeeling) {
        this.temperature_howfeeling = temperature_howfeeling;
    }

    public void setAtm_pressure(String atm_pressure) {
        this.atm_pressure = atm_pressure;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public void setMoonrise(String moonrise) {
        this.moonrise = moonrise;
    }

    public void setMoonset(String moonset) {
        this.moonset = moonset;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public void setCloudcover(String cloudcover) {
        this.cloudcover = cloudcover;
    }

    public void setChanceofrain(String chanceofrain) {
        this.chanceofrain = chanceofrain;
    }

    public void setChanceoffog(String chanceoffog) {
        this.chanceoffog = chanceoffog;
    }

    public void setWind(String wind) {
        this.wind = wind;
    }
}
