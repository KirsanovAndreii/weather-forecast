package com.example.ankir.weatherforecast;

import com.example.ankir.weatherforecast.model.Response;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Volodymyr on 01.07.2017.
 */

public class Retrofit {

    private static final String ENDPOINT = "http://api.worldweatheronline.com/premium/v1/";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/weather.ashx")
        void getResponse(@Query("key") String key,
                 @Query("q") String q,
                 @Query("format") String format,
                 @Query("num_of_days") String num_of_days,
                 @Query("tp") String tp,
                 @Query("lang") String lang,
                 Callback<Response> callback);
    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getWeather(Callback<Response> callback, String currentCity) {
        apiInterface.getResponse("a6d3b3c618684f839a975110170107", currentCity, "json", "5", "24", "ru", callback);
    }
}
