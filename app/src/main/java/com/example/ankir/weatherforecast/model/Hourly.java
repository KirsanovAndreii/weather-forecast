
package com.example.ankir.weatherforecast.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Hourly {

    public String time;
    public String tempC;
    public String windspeedMiles;
    public String windspeedKmph;
    public String winddirDegree;
    public String winddir16Point;
    public String weatherCode;
    public List<WeatherIconUrl> weatherIconUrl = null;
    public List<WeatherDesc> weatherDesc = null;
    public List<Lang_ru> lang_ru = null;

    public String precipMM;
    public String humidity;
    public String visibility;
    public String pressure;
    public String cloudcover;
    public String heatIndexC;
    public String heatIndexF;
    public String dewPointC;
    public String dewPointF;
    public String windChillC;
    public String windChillF;
    public String windGustMiles;
    public String windGustKmph;
    public String FeelsLikeC;
    public String FeelsLikeF;
    public String chanceofrain;
    public String chanceofremdry;
    public String chanceofwindy;
    public String chanceofovercast;
    public String chanceofsunshine;
    public String chanceoffrost;
    public String chanceofhightemp;
    public String chanceoffog;
    public String chanceofsnow;
    public String chanceofthunder;

}
