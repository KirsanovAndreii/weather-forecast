package com.example.ankir.weatherforecast;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ankir.weatherforecast.model.ModelForAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ankir on 01.07.2017.
 */



    public class AdapterList extends ArrayAdapter<ModelForAdapter> {
    Context context;

      public AdapterList(@NonNull Context context, @LayoutRes List<ModelForAdapter> values) {
            super(context, R.layout.item_view, values);
          this.context = context;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            ViewHolder holder;
            View rowView = convertView;
            if (rowView == null) {
                rowView = LayoutInflater.from(getContext()).inflate(R.layout.item_view, parent, false);
                holder = new ViewHolder();
                holder.icon_wether = (ImageView) rowView.findViewById(R.id.icon_wether);
                holder.weather = (TextView) rowView.findViewById(R.id.weather);
                holder.temperature = (TextView) rowView.findViewById(R.id.temperature);
                holder.temperature_howfeeling = (TextView) rowView.findViewById(R.id.temperature_howfeeling);
                holder.atm_pressure = (TextView) rowView.findViewById(R.id.atm_pressure);
                holder.date = (TextView) rowView.findViewById(R.id.date);

                rowView.setTag(holder);
            } else {
                holder = (ViewHolder) rowView.getTag();
            }
        //    holder.icon_wether.setImageResource(R.drawable.i);

            Picasso.with(context)
                    .load(getItem(position).getIcon_wether())
                    .placeholder(R.drawable.i)
                    .error(R.drawable.i)
                    .into(holder.icon_wether);

            holder.weather.setText(getItem(position).getWeather());
            holder.temperature.setText(getItem(position).getTemperature());
            holder.temperature_howfeeling.setText(getItem(position).getTemperature_howfeeling());
            holder.atm_pressure.setText(getItem(position).getAtm_pressure());
            holder.date.setText(getItem(position).getDate());

            return rowView;
        }

        class ViewHolder {
            public ImageView icon_wether;
            public TextView weather;
            public TextView temperature;
            public TextView temperature_howfeeling;
            public TextView atm_pressure;
            public TextView date;
        }
    }

