package com.example.ankir.weatherforecast;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ankir.weatherforecast.model.ModelForAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityWeatherFor1Day extends AppCompatActivity {
    ModelForAdapter weatherOneDay;

    @BindView(R.id.second_icon_wether)
    ImageView icon_wether;
    @BindView(R.id.second_weather)
    TextView weather;
    @BindView(R.id.second_temperature)
    TextView temperature;
    @BindView(R.id.second_temperature_howfeeling)
    TextView temperature_howfeeling;
    @BindView(R.id.second_atm_pressure)
    TextView atm_pressure;
    @BindView(R.id.second_sun_rise)
    TextView sun_rise;
    @BindView(R.id.second_sun_set)
    TextView sun_set;
    @BindView(R.id.second_moon_rise)
    TextView moon_rise;
    @BindView(R.id.second_moon_set)
    TextView moon_set;
    @BindView(R.id.second_humidity)
    TextView humidity;
    @BindView(R.id.second_cloudcover)
    TextView cloudcover;
    @BindView(R.id.second_chanceofrain)
    TextView chanceofrain;
    @BindView(R.id.second_chanceoffog)
    TextView chanceoffog;
    @BindView(R.id.second_wind)
    TextView wind;
    @BindView(R.id.date)
    TextView date;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_for1_day);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        weatherOneDay = (ModelForAdapter) intent.getSerializableExtra(MainActivity.KEY_WEATHER);
    //    icon_wether.setImageResource(R.drawable.i);

        Picasso.with(this)
                .load(weatherOneDay.getIcon_wether())
                .placeholder(R.drawable.i)
                .error(R.drawable.i)
                .into(icon_wether);


        weather.setText(weatherOneDay.getWeather());
        temperature.setText(weatherOneDay.getTemperature());
        temperature_howfeeling.setText(weatherOneDay.getTemperature_howfeeling());
        atm_pressure.setText(weatherOneDay.getAtm_pressure());
        sun_rise.setText(weatherOneDay.getSunrise());
        sun_set.setText(weatherOneDay.getSunset());
        moon_rise.setText(weatherOneDay.getMoonrise());
        moon_set.setText(weatherOneDay.getMoonset());
        humidity.setText(weatherOneDay.getHumidity());
        cloudcover.setText(weatherOneDay.getCloudcover());
        chanceofrain.setText(weatherOneDay.getChanceofrain());
        chanceoffog.setText(weatherOneDay.getChanceoffog());
        wind.setText(weatherOneDay.getWind());
        date.setText(weatherOneDay.getDate());
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // получим идентификатор выбранного пункта меню
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(ActivityWeatherFor1Day.this, Options.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
