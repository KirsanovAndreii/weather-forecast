package com.example.ankir.weatherforecast;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Options extends AppCompatActivity {

    private static String cityPrompt;
    @BindView(R.id.cityPrompt)
    EditText et;

    private static boolean isCelsium;
    @BindView( R.id.isCelsium)
    RadioButton rb;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        ButterKnife.bind(this);
        isCelsium = rb.isChecked();
        cityPrompt = et.getText().toString();
    }
}
